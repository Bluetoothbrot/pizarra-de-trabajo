<h1 id="cover-page" align="center"> Software Engineering Fall 2019 </h1>
<h2 align="center"> Dra. Ofelia Cervantes </h2>

<h1 id="title" align="center"> Sprint 1 </h1>
<!--<h2 id="members"> Members: </h2>-->

<h3 id="member" align="center">154816 Jose Angel Hernandez Morales  </h3>
<h3 id="member" align="center">157130 Mariella Sanchez Alvarez  </h3>
<h3 id="member" align="center">154409 Antonio Rafael Bravo Pacheco  </h3>
<h3 id="member" align="center">155150 Jose Carlos Archundia Adriano  </h3>
<h3 id="member" align="center">161035 Hector Eduardo Cardona Espinoza </h3>
<h3 id="member" align="center">403028 Oliver Maicher  </h3>

<br>

---

# Index
<!-- TOC depthFrom:1 depthTo:3 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Index](#index)
- [Company Description](#company-description)
	- [Presentation](#presentation)
	- [Capacities and Roles](#capacities-and-roles)
	- [Version Management Tools](#version-management-tools)
	- [Project Management Tools](#project-management-tools)
	- [Hardware and Software Selected for Implementing the Project](#hardware-and-software-selected-for-implementing-the-project)
	- [Individual Contributions](#individual-contributions)
- [First Actions - Artifacts](#first-actions-artifacts)
	- [SW PRODUCT](#sw-product)
		- [Context](#context)
		- [Proposal](#proposal)
		- [Executive Summary](#executive-summary)
	- [Proto-Personas](#proto-personas)
		- [Contractor](#contractor)
		- [Employee](#employee)
	- [Story Boards](#story-boards)
		- [Contractor:](#contractor)
		- [Employee:](#employee)
- [Product Backlog:](#product-backlog)
- [Appendix](#appendix)

<!-- /TOC -->

<br>

---



# Company Description


## Presentation
_At Aguacate, we aim to provide software that caters to our clients needs. We believe in fast, efficient software that not only meets our clients needs, but also enables them to do their best work. Our team of experienced developers is able to pinpoint exactly what our client needs, and create an adequate solution for any problem._

## Capacities and Roles
* Scrum Master  -  Jose Carlos
* Product Owner -  Mariella

## Version Management Tools
[GitLab](https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo) - For collaboration and management
![GitLab Members](../Artifacts/img/gitlabMembers.png)

## Project Management Tools
[GitLab](https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo) - For task assignment and organization using Kanban  
[Trello](https://trello.com/b/Qc53onQ4/pizarra-de-trabajo) - Complement to GitLab Kanban

## Hardware and Software Selected for Implementing the Project
Atom - Used as a lightweight code editor  
VSCode - Used as a fully-featured code editor and IDE  
Team computers and cell phones - Used by the team to develop and collaborate on the software  

## Individual Contributions
Jose Carlos -  
Jose Angel -  
Mariella -  
Hector -  
Rafael -  
Oliver -  

---


# First Actions - Artifacts

## SW PRODUCT

### Context
Carlos is a student that wants some extra money, but does not want to commit to a full-time job.  
Daniel is a restaurant owner who needs an extra worker for an event he is hosting, but only wants to hire for a day or two.

### Proposal
We propose a web-based platform where we can connect two types of users: Contractors and Employees. Our platform provides a bridge between these two users that allows them to coordinate short time job listings, similar to the way a Temp Agency functions.

### Executive Summary
The objective of our software product is to create a platform where users can post or apply to work proposals. Users called Contractors, users who post work, can choose between the different users called Employees, users who apply to work, who are interested in doing the work. Our goal is to guarantee the Contractors a suitable Employee who will complete the work given to them, and guarantee the Employee his payment. To do this,we will manage payments on our platform, asking Contractors to make a deposit beforehand. This deposit will be help until the verification of completed work. If the task assigned to the Employee is completed successfully, the money deposited by the Contractor will be transferred to the Employee, otherwise, either the Employee or the Contractor can open up a claim to dispute the payment.  Our platform will allow Contractors to filter through Employee candidates with suitable skills to the job the Contractor has posted. We aim to provide a secure platform that enables a Contractor to connect with an Employee to realize a task suitable to the Employee’s skill set.

## Proto-Personas

### Contractor
* Daniel
* 30
* Male
* Restaurant Manager
* Good with technology
* Has enough money to hire others to work for him
* Organizing Octoberfest in Mexico
* Needs reliable workers for the evening



### Employee
* Carlos
* 23
* Male
* In need of money for vacation
* Looking for a weekend job
* Tech savy
* Physically able
* Not picky with work
* Has transport

## Story Boards
### Contractor:  
![Contractor Story board](../Artifacts/Storyboards/storyboardContractor.jpeg)  
In this story board we illustrate the process of a Contractor posting a job listing on our platform. The Contractor is in need of an employee and posts a job offer to our platform, after which he receives an Employee which fulfills his requirements.  

### Employee:  
![Employee Story board](../Artifacts/Storyboards/storyboardEmployee.png)  
In this story board we illustrate the process of an Employee looking for suitable work on our platform. The Employee is in need of money and consults our platform for jobs that are suitable for him, after which he applies and is accepted to a job where he will be paid.


# [Product Backlog](../Artifacts/PRODUCTBACKLOG.md)
{{../Artifacts/PRODUCTBACKLOG.md}}

### Sprint 1 Backlog:
| Tasks |
| ----- |
| Guarantee that the work will be done correctly |
| Verify work experience |
| Must be able to work according tot he Contractor's schedule |
| Employee specific information |


# Appendix
[ {{../Artifacts/CVs/Curriculúm1.pdf}} ](../Artifacts/CVs/Curriculúm1.pdf)  
[ {{../Artifacts/CVs/CV_154816.pdf}} ](../Artifacts/CVs/CV_154816.pdf)  
[ {{../Artifacts/CVs/CV_Oliver_Manchier_WS18_19.pdf}} ](../Artifacts/CVs/CV_Oliver_Manchier_WS18_19.pdf)  
[ {{../Artifacts/CVs/JoseCarlos.pdf}} ](../Artifacts/CVs/JoseCarlos.pdf)  
[ {{../Artifacts/CVs/Rafael.pdf}} ](../Artifacts/CVs/Rafael.pdf)  
