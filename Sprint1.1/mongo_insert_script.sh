#! /usr/bin/env bash
#script for webshop environment installation by Oliver Maicher, 24.09.2019
#content delivered by Oliver Maicher

# hashtag # is an illegal character in mongo shell: 2019-05-09T10:44:03.317+0200 E QUERY    [js] SyntaxError: illegal character @(shell):1:0
# there's this error message however it works anyway

mongo mongodb://localhost:27017/pizarro <<EOF

use pizarro;

db.dropDatabase();
db.dropUser("pizarro-user")

use pizarro;

db.createUser(
  {
    "user": "pizarro-user",
    "pwd": "pizarro-pw",
    "roles": [
       { "role": "read", "db": "pizarro" },
       { "role": "readWrite", "db": "pizarro" }
    ]
  }
);

db.createCollection("jobs");
db.createCollection("users");

db.users.insertMany([
  {
    "name" :"José Luis Zechinelli Martini",
    "role": "Employer",
    "verified": true,
    "email" : "joseluis.zechinelli@udlap.mx",
    "password" : "password",
    "address": "Universidad de las Américas Puebla. Ex Hacienda Sta. Catarina Mártir S/N. San Andrés Cholula, Puebla. C.P. 72810. México",
    "age": "20",
    "phoneNumber": "+5215902626292",
    "userId": "1",
    "rating": "10"
  },
  {
    "name" :"Jose Carlos Archundia",
    "role": "Employee",
    "skills": [{"name":"Crime scene cleaning", "category":"Cleaning", "description":"Professional crime scene cleaning practice", "approved": true, "rating": 10},
                {"name":"Precious metal processing", "category":"Crafts", "description":"Manual processing of precious metals", "approved": false, "rating": 0}],
    "verified": false,
    "email" : "jarchund@gmail.com",
    "password" : "password",
    "address": "Universidad de las Américas Puebla. Ex Hacienda Sta. Catarina Mártir S/N. San Andrés Cholula, Puebla. C.P. 72810. México",
    "age": "20",
    "phoneNumber": "+5215902626181",
    "userId": "2",
    "rating": "0"
  },
  {
    "name":"Hector",
    "role": "admin",
    "verfied": true,
    "email" : "hectorit@udlap.mx",
    "password" : "password",
    "address": "Universidad de las Américas Puebla. Ex Hacienda Sta. Catarina Mártir S/N. San Andrés Cholula, Puebla. C.P. 72810. México",
    "age": "20",
    "phoneNumber": "+5215902626070",
    "userId": "3",
    "rating": "0"
  }
]
);

db.jobs.insert(
  {
    "jobName": "Cleaning private house",
    "jobId" : "1",
    "charge": "100",
    "hoursOfWork": "4",
    "category": "Cleansing",
    "description": "Gotta clean someone else's...",
    "employerId": "1",
    "employeeId": "",
    "amountAvailable": "1"
  }
);


EOF
