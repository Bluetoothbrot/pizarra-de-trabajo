rabbit.data.layerStore.addLayerFromHtml('<div xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" id="__containerId__page696252306-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page696252306" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page696252306-layer-rect717243194" style="position: absolute; left: 0px; top: 0px; width: 415px; height: 660px" data-stencil-type="static.rect" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect717243194" data-review-reference-id="rect717243194">\
            <div class="stencil-wrapper" style="width: 415px; height: 660px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" overflow="hidden" style="height: 660px; width:415px;" width="415" height="660" viewBox="0 0 415 660">\
                     <g width="415" height="660">\
                        <rect x="0" y="0" width="415" height="660" style="stroke-width:1;stroke:black;fill:white;"></rect>\
                     </g>\
                  </svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-icon213650403" style="position: absolute; left: 140px; top: 10px; width: 132px; height: 132px" data-stencil-type="fonticon.icon" data-interactive-element-type="fonticon.icon" class="icon stencil mobile-interaction-potential-trigger " data-stencil-id="icon213650403" data-review-reference-id="icon213650403">\
            <div class="stencil-wrapper" style="width: 132px; height: 132px">\
               <div xmlns="http://www.w3.org/1999/xhtml" xmlns:pidoco="http://www.pidoco.com/util" style="width:132px;height:132px;" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" width="132" height="132" class="fill-black">\
                     <svg style="position: absolute; width: 0; height: 0;" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e228" preserveAspectRatio="xMidYMid meet">\
\
<path d="M249.241 613.549h142.262q0 90.315 87.375 116.13v-197.863q-2.621-1.033-8.103-2.383t-8.103-2.304q-24.148-6.911-42.495-13.186t-40.191-16.283-37.73-20.813-31.612-26.689-25.498-33.919-15.886-42.495-6.117-52.821q0-39 11.915-72.758t32.25-58.304 48.134-42.813 58.78-28.992 64.658-14.218v-52.982h66.246v53.616q40.986 4.291 74.586 14.932t62.274 29.786 48.293 46.15 30.582 65.849 10.961 87.534h-142.658q-1.351-26.769-5.322-44.799t-13.027-33.282-25.181-24.464-40.51-13.583v172.763q14.853 1.668 22.639 3.496t13.9 4.448 11.121 3.972q47.341 12.869 69.502 22.161 133.048 55.284 133.048 189.286 0 46.705-16.045 86.581t-46.864 70.455-78.637 49.882-108.662 23.83v51.948h-66.246v-53.616q-47.659-5.959-89.837-26.372t-73.316-51.789-48.93-74.428-17.554-91.665zM405.402 307.737q0 28.119 14.932 42.972t58.543 29.786v-152.191q-32.091 5.958-52.744 25.656t-20.731 53.775zM545.124 735q51.63-4.607 75.62-24.307t23.988-63.705q0-36.777-23.83-57.429t-75.779-34.951v180.39z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e208" preserveAspectRatio="xMidYMid meet">\
\
<path d="M214.133 681.384q0-7.625 5.639-13.264l186.984-186.984-186.984-186.984q-5.639-5.639-5.639-13.583t5.639-13.264l78.797-78.717q5.561-5.322 13.186-5.322t13.264 5.322l187.299 186.984 186.984-186.984q5.322-5.322 13.264-5.322t13.264 5.322l78.717 78.717q5.322 5.322 5.322 13.264t-5.322 13.583l-186.665 186.984 186.665 186.665q5.322 5.639 5.322 13.424t-5.322 13.424l-78.717 78.717q-5.322 5.639-13.264 5.639t-13.583-5.639l-186.665-186.665-186.984 186.984q-5.322 5.322-13.264 5.322t-13.583-5.322l-78.717-79.114q-5.639-5.561-5.639-13.185z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e004" preserveAspectRatio="xMidYMid meet">\
\
<path d="M131.364 828.65v-77.764q0-26.451 6.832-39.875t26.292-24.307q159.181-92.061 231.704-123.515v-124.388q-33.124-24.544-33.124-74.191v-98.575q0-41.702 16.045-74.824t50.519-53.617 82.371-20.493 82.45 20.493 50.439 53.617 16.045 74.824v98.575q0 49.327-33.124 73.793v124.788q61.559 25.181 231.704 123.515 19.539 10.883 26.292 24.307t6.832 39.875v77.764q0 6.99-4.846 11.756t-11.756 4.765h-728.070q-6.674 0-11.597-4.765t-5.004-11.756z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e028" preserveAspectRatio="xMidYMid meet">\
\
<path d="M123.103 406.312q0-63.863 25.021-121.928t67.041-100.163 100.242-66.96 122.165-25.021 121.927 25.021 100.083 66.959 67.041 100.163 25.022 121.927q0 92.061-50.36 170.143l194.607 194.607q5.004 4.607 5.004 11.597t-5.004 11.915l-70.455 70.455q-4.607 4.687-11.756 4.687t-11.756-4.687l-194.607-194.607q-77.445 50.36-169.745 50.36-63.942 0-122.165-25.022t-100.242-67.041-67.041-100.242-25.021-122.165zM223.106 406.312q0 43.687 16.999 83.404t45.833 68.39 68.39 45.673 83.244 16.999 83.165-16.999 68.39-45.673 45.833-68.39 17.077-83.404q0-43.29-17.077-83.007t-45.833-68.39-68.39-45.672-83.165-16.999-83.244 16.999-68.39 45.672-45.833 68.39-17.077 83.007z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e195" preserveAspectRatio="xMidYMid meet">\
\
<path d="M106.58 481.138q0-55.284 14.535-107.709t40.748-96.986 63.545-81.894 81.894-63.545 97.144-40.748 107.552-14.535q82.45 0 157.513 32.091t129.474 86.343 86.343 129.474 32.091 157.513-32.091 157.513-86.343 129.474-129.474 86.343-157.513 32.091q-54.969 0-107.552-14.535t-97.144-40.748-81.894-63.545-63.545-81.894-40.748-96.985-14.535-107.709zM352.819 389.79q0 3.652 2.304 5.959 1.987 2.621 5.958 2.621h89.043q7.625 0 8.262-7.548 1.668-24.544 15.569-37.095t39.081-12.629q17.872 0 29.071 10.645t11.28 27.166q0 9.214-4.291 18.031t-11.28 15.33-10.247 9.294-6.592 5.004q-58.62 42.656-58.62 95.318v33.759q0 1.589 3.337 4.926t4.926 3.337h84.755q3.337 0 5.799-2.304t2.463-5.639q0.635-31.139 9.612-47.977t33.124-32.487q16.203-10.247 24.464-16.204t16.521-14.218 11.597-17.237 5.322-21.368 1.986-30.264q0-31.773-13.424-56.872t-35.745-40.43-49.009-22.955-55.046-7.784q-72.839 0-114.699 37.015t-45.514 104.611zM445.833 729.359q0 6.99 4.765 11.756t11.756 4.765h100.957q6.592 0 11.597-4.765t4.926-11.756v-99.289q0-6.911-4.926-11.756t-11.597-4.765h-100.957q-6.911 0-11.756 4.765t-4.765 11.756v99.289z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e340" preserveAspectRatio="xMidYMid meet">\
\
<path d="M82.434 442.452q-1.351-19.223 9.93-37.73t32.091-22.875q32.726-6.911 62.037-1.668t40.908 21.208q36.38-26.531 81.894-35.269t91.028-0.953 91.187 29.23q61.242 29.152 87.057 37.413 27.404 8.659 42.336 5.958 10.564-1.987 18.19-6.275t12.075-10.723 7.148-12.075 5.799-13.9 5.4-12.948q-30.739 1.351-64.658-3.652t-60.766-13.027-49.168-16.283-34.551-14.059l-11.915-5.639q-3.972-1.589-9.929-4.765t-16.362-11.915-11.915-17.237 13.583-18.984 50.121-18.904q19.857-4.607 42.020-5.242t43.529 2.621 42.020 8.42 40.51 12.629 36.3 14.376 31.612 14.853 23.988 12.948 16.045 9.612l1.27-0.715q-10.883-10.883-25.099-23.512t-43.051-32.408-57.747-33.919-67.516-22.32-74.268-3.337q-4.687-4.291-8.817-9.453t-7.229-11.201-3.972-11.915 2.145-11.121 9.929-8.897 20.652-5.481 33.282-0.477q26.134 1.589 54.094 11.201t51.472 23.669 45.991 30.66 39.716 32.886 30.264 29.787 20.017 21.685l6.99 8.261q3.652 2.383 20.017 8.5t39.397 18.666 49.486 33.124q23.114 18.19 34.235 28.437t16.918 21.526 4.131 21.368-8.975 26.292q-4.291 10.247-8.738 16.84t-7.625 8.975-6.275 2.778-4.527-0.16-2.621-1.668q14.218 15.886 1.351 26.212-11.28 8.897-28.993 12.233t-35.904 2.939-36.062-0.635-33.282 3.099-23.669 12.788q-15.886 17.237-32.091 44.482t-28.277 52.346-30.66 55.761-38.047 53.617q5.004 1.987 13.9 4.291t16.443 4.448 14.535 4.846 10.883 6.275 3.178 7.944-2.778 7.944-5.959 5.799-7.625 3.812-9.77 2.145-10.247 0.953-11.597 0.16-11.121-0.635-11.041-0.874-9.612-0.795q-67.516 53.299-96.35 54.969-12.55 0.953-11.915-8.975 1.033-14.853 25.814-36.38 5.322-5.004 11.597-9.612-16.204-1.668-56.078-1.351t-71.489-2.463-49.804-12.39q-15.886-8.578-30.264-25.181t-23.353-32.091-21.208-31.773-24.148-23.83q-6.592-4.291-19.064-9.77t-22.161-10.405-20.493-12.55-18.507-19.699-11.756-28.357q-10.961-43.687-11.439-57.589t8.42-61.56q-1.987-6.911-9.294-7.944t-15.886 1.509-18.19 3.018-16.521-3.496q-11.28-7.309-12.55-26.451zM370.691 598.616q2.939 1.033 4.607 1.668t8.659 4.765 11.915 9.135 12.075 14.535 11.915 21.208 8.578 29.152 4.448 38.048q12.947 3.257 54.969 12.233t72.122 17.237q8.262-15.251 12.39-33.124t4.21-32.567-1.192-27.325-3.177-19.539l-1.668-7.309q-2.939 1.351-8.261 3.496t-21.286 5.959-31.455 4.607-35.745-3.972-37.254-16.362q-35.109-24.148-60.925-23.195-4.926 0.397-4.926 1.351zM801.926 389.79q-3.337 8.578 2.145 17.554t16.759 13.264q10.883 3.972 20.813 0.795t13.264-11.756-2.304-17.554-16.601-12.868q-10.883-4.291-20.971-1.192t-13.107 11.756z"/>\
</symbol></defs></svg>\
                     <!--load fonticon glyph-e004-->\
                     <use xlink:href="#icon-glyph-e004"></use>\
                  </svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-text46290819" style="position: absolute; left: 165px; top: 145px; width: 87px; height: 69px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text46290819" data-review-reference-id="text46290819">\
            <div class="stencil-wrapper" style="width: 87px; height: 69px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:97px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.text2">\
                        <div style="text-align: center;"><span style="color: black; font-size: 20px;">John Doe<br />Male<br />22</span></div></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-rect821839070" style="position: absolute; left: 415px; top: 0px; width: 951px; height: 50px" data-stencil-type="static.rect" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect821839070" data-review-reference-id="rect821839070">\
            <div class="stencil-wrapper" style="width: 951px; height: 50px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" overflow="hidden" style="height: 50px; width:951px;" width="951" height="50" viewBox="0 0 951 50">\
                     <g width="951" height="50">\
                        <rect x="0" y="0" width="951" height="50" style="stroke-width:1;stroke:black;fill:white;"></rect>\
                     </g>\
                  </svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-tabbutton56204643" style="position: absolute; left: 420px; top: 5px; width: 95px; height: 45px" data-stencil-type="default.tabbutton" data-interactive-element-type="default.tabbutton" class="tabbutton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton56204643" data-review-reference-id="tabbutton56204643">\
            <div class="stencil-wrapper" style="width: 95px; height: 45px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 55px;width:105px;" width="95" height="50">\
                     <g id="target" x="-5" y="0" width="95" height="45" name="target" class="">\
                        <path id="__containerId__-page696252306-layer-tabbutton56204643_small_path" width="95" height="45" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 90,5 C 100,5 100,15 100,15 L 100,50 L 5,50 L 5,15"></path>\
                     </g>\
                  </svg>\
                  <div id="__containerId__-page696252306-layer-tabbutton56204643div" class="helvetica-font" style="position: absolute; top: 4px; height: 45px;width:95px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:12.5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'__containerId__-page696252306-layer-tabbutton56204643\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'__containerId__-page696252306-layer-tabbutton56204643\', \'result\');">\
                     				Personal Info\
                     				\
                     <addMouseOverListener></addMouseOverListener>\
                     				\
                     <addMouseOutListener></addMouseOutListener>\
                     			\
                  </div>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 95px; height: 45px"></div>\
            <div xmlns:json="http://json.org/" xmlns="http://www.w3.org/1999/xhtml"><script type="text/javascript">\
				$(document).ready(function(){\
					rabbit.errorContext(function () {\
						rabbit.interaction.manager.registerInteraction(\'__containerId__-page696252306-layer-tabbutton56204643\', \'interaction295871037\', {"button":"left","id":"action216070513","numberOfFinger":"1","type":"click"},  \
							[\
								{"delay":"0","id":"reaction107170153","layer":"layer483188662","type":"toggleLayer","visibility":"show"}\
							]\
						);\
					});\
				});\
			</script></div>\
            <div xmlns:json="http://json.org/" xmlns="http://www.w3.org/1999/xhtml"><script type="text/javascript">\
				$(document).ready(function(){\
					rabbit.errorContext(function () {\
						rabbit.interaction.manager.registerInteraction(\'__containerId__-page696252306-layer-tabbutton56204643\', \'interaction773099019\', {"button":"left","id":"action913408402","numberOfFinger":"1","type":"click"},  \
							[\
								{"delay":"0","id":"reaction485573448","layer":"layer10959018","type":"toggleLayer","visibility":"hide"}\
							]\
						);\
					});\
				});\
			</script></div>\
         </div>\
         <div id="__containerId__-page696252306-layer-tabbutton578127298" style="position: absolute; left: 535px; top: 5px; width: 95px; height: 45px" data-stencil-type="default.tabbutton" data-interactive-element-type="default.tabbutton" class="tabbutton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton578127298" data-review-reference-id="tabbutton578127298">\
            <div class="stencil-wrapper" style="width: 95px; height: 45px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 55px;width:105px;" width="95" height="50">\
                     <g id="target" x="-5" y="0" width="95" height="45" name="target" class="">\
                        <path id="__containerId__-page696252306-layer-tabbutton578127298_small_path" width="95" height="45" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 90,5 C 100,5 100,15 100,15 L 100,50 L 5,50 L 5,15"></path>\
                     </g>\
                  </svg>\
                  <div id="__containerId__-page696252306-layer-tabbutton578127298div" class="helvetica-font" style="position: absolute; top: 4px; height: 45px;width:95px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:12.5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'__containerId__-page696252306-layer-tabbutton578127298\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'__containerId__-page696252306-layer-tabbutton578127298\', \'result\');">\
                     				Skills\
                     				\
                     <addMouseOverListener></addMouseOverListener>\
                     				\
                     <addMouseOutListener></addMouseOutListener>\
                     			\
                  </div>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 95px; height: 45px"></div>\
            <div xmlns:json="http://json.org/" xmlns="http://www.w3.org/1999/xhtml"><script type="text/javascript">\
				$(document).ready(function(){\
					rabbit.errorContext(function () {\
						rabbit.interaction.manager.registerInteraction(\'__containerId__-page696252306-layer-tabbutton578127298\', \'interaction353125840\', {"button":"left","id":"action131831956","numberOfFinger":"1","type":"click"},  \
							[\
								{"delay":"0","id":"reaction819582422","layer":"layer10959018","type":"toggleLayer","visibility":"toggle"}\
							]\
						);\
					});\
				});\
			</script></div>\
            <div xmlns:json="http://json.org/" xmlns="http://www.w3.org/1999/xhtml"><script type="text/javascript">\
				$(document).ready(function(){\
					rabbit.errorContext(function () {\
						rabbit.interaction.manager.registerInteraction(\'__containerId__-page696252306-layer-tabbutton578127298\', \'interaction83456576\', {"button":"left","id":"action691216808","numberOfFinger":"1","type":"click"},  \
							[\
								{"delay":"0","id":"reaction61523874","layer":"layer483188662","type":"toggleLayer","visibility":"hide"}\
							]\
						);\
					});\
				});\
			</script></div>\
         </div>\
         <div id="__containerId__-page696252306-layer-tabbutton775190753" style="position: absolute; left: 660px; top: 5px; width: 112px; height: 45px" data-stencil-type="default.tabbutton" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton775190753" data-review-reference-id="tabbutton775190753">\
            <div class="stencil-wrapper" style="width: 112px; height: 45px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 55px;width:122px;" width="112" height="50">\
                     <g id="target" x="-5" y="0" width="112" height="45" name="target" class="">\
                        <path id="__containerId__-page696252306-layer-tabbutton775190753_small_path" width="112" height="45" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 107,5 C 117,5 117,15 117,15 L 117,50 L 5,50 L 5,15"></path>\
                     </g>\
                  </svg>\
                  <div id="__containerId__-page696252306-layer-tabbutton775190753div" class="helvetica-font" style="position: absolute; top: 4px; height: 45px;width:112px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:12.5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'__containerId__-page696252306-layer-tabbutton775190753\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'__containerId__-page696252306-layer-tabbutton775190753\', \'result\');">\
                     				Payment Options\
                     				\
                     <addMouseOverListener></addMouseOverListener>\
                     				\
                     <addMouseOutListener></addMouseOutListener>\
                     			\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-tabbutton664702650" style="position: absolute; left: 815px; top: 5px; width: 108px; height: 45px" data-stencil-type="default.tabbutton" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton664702650" data-review-reference-id="tabbutton664702650">\
            <div class="stencil-wrapper" style="width: 108px; height: 45px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 55px;width:118px;" width="108" height="50">\
                     <g id="target" x="-5" y="0" width="108" height="45" name="target" class="">\
                        <path id="__containerId__-page696252306-layer-tabbutton664702650_small_path" width="108" height="45" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 103,5 C 113,5 113,15 113,15 L 113,50 L 5,50 L 5,15"></path>\
                     </g>\
                  </svg>\
                  <div id="__containerId__-page696252306-layer-tabbutton664702650div" class="helvetica-font" style="position: absolute; top: 4px; height: 45px;width:108px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:12.5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'__containerId__-page696252306-layer-tabbutton664702650\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'__containerId__-page696252306-layer-tabbutton664702650\', \'result\');">\
                     				Account settings\
                     				\
                     <addMouseOverListener></addMouseOverListener>\
                     				\
                     <addMouseOutListener></addMouseOutListener>\
                     			\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-rating107575756" style="position: absolute; left: 175px; top: 255px; width: 150px; height: 30px" data-stencil-type="default.rating" data-interactive-element-type="default.rating" class="rating stencil mobile-interaction-potential-trigger " data-stencil-id="rating107575756" data-review-reference-id="rating107575756">\
            <div class="stencil-wrapper" style="width: 150px; height: 30px">\
               <div xmlns="http://www.w3.org/1999/xhtml" style="width:150px; height:30px" onmouseout="if(rabbit.stencils.rating.checkMouseOutDiv(\'__containerId__-page696252306-layer-rating107575756\', event)) rabbit.facade.raiseEvent(rabbit.events.ratingMouseOut, \'__containerId__-page696252306-layer-rating107575756\');" title=""><img id="__containerId__-page696252306-layer-rating107575756-1" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 0px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating107575756\', \'1\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating107575756\', \'1\');" /><img id="__containerId__-page696252306-layer-rating107575756-2" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 30px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating107575756\', \'2\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating107575756\', \'2\');" /><img id="__containerId__-page696252306-layer-rating107575756-3" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 60px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating107575756\', \'3\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating107575756\', \'3\');" /><img id="__containerId__-page696252306-layer-rating107575756-4" src="../resources/icons/rating_white.png" width="30" height="30" style="position: absolute; left: 90px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating107575756\', \'4\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating107575756\', \'4\');" /><img id="__containerId__-page696252306-layer-rating107575756-5" src="../resources/icons/rating_white.png" width="30" height="30" style="position: absolute; left: 120px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating107575756\', \'5\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating107575756\', \'5\');" /></div><script xmlns="http://www.w3.org/1999/xhtml" type="text/javascript">rabbit.errorContext(function () {\
			rabbit.stencils.rating.onLoad("__containerId__-page696252306-layer-rating107575756", "3");\
		});</script></div>\
         </div>\
         <div id="__containerId__-page696252306-layer-rating484031601" style="position: absolute; left: 175px; top: 300px; width: 150px; height: 30px" data-stencil-type="default.rating" data-interactive-element-type="default.rating" class="rating stencil mobile-interaction-potential-trigger " data-stencil-id="rating484031601" data-review-reference-id="rating484031601">\
            <div class="stencil-wrapper" style="width: 150px; height: 30px">\
               <div xmlns="http://www.w3.org/1999/xhtml" style="width:150px; height:30px" onmouseout="if(rabbit.stencils.rating.checkMouseOutDiv(\'__containerId__-page696252306-layer-rating484031601\', event)) rabbit.facade.raiseEvent(rabbit.events.ratingMouseOut, \'__containerId__-page696252306-layer-rating484031601\');" title=""><img id="__containerId__-page696252306-layer-rating484031601-1" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 0px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating484031601\', \'1\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating484031601\', \'1\');" /><img id="__containerId__-page696252306-layer-rating484031601-2" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 30px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating484031601\', \'2\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating484031601\', \'2\');" /><img id="__containerId__-page696252306-layer-rating484031601-3" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 60px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating484031601\', \'3\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating484031601\', \'3\');" /><img id="__containerId__-page696252306-layer-rating484031601-4" src="../resources/icons/rating_white.png" width="30" height="30" style="position: absolute; left: 90px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating484031601\', \'4\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating484031601\', \'4\');" /><img id="__containerId__-page696252306-layer-rating484031601-5" src="../resources/icons/rating_white.png" width="30" height="30" style="position: absolute; left: 120px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page696252306-layer-rating484031601\', \'5\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page696252306-layer-rating484031601\', \'5\');" /></div><script xmlns="http://www.w3.org/1999/xhtml" type="text/javascript">rabbit.errorContext(function () {\
			rabbit.stencils.rating.onLoad("__containerId__-page696252306-layer-rating484031601", "3");\
		});</script></div>\
         </div>\
         <div id="__containerId__-page696252306-layer-text40553976" style="position: absolute; left: 60px; top: 260px; width: 108px; height: 17px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text40553976" data-review-reference-id="text40553976">\
            <div class="stencil-wrapper" style="width: 108px; height: 17px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:118px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.text2">\
                        <p style="font-size: 14px;">Employee Rating</p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-text698395343" style="position: absolute; left: 60px; top: 305px; width: 105px; height: 17px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text698395343" data-review-reference-id="text698395343">\
            <div class="stencil-wrapper" style="width: 105px; height: 17px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:115px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.text2">\
                        <p style="font-size: 14px;">Employer Rating</p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-icon640353453" style="position: absolute; left: 1320px; top: 10px; width: 32px; height: 32px" data-stencil-type="fonticon.icon" data-interactive-element-type="fonticon.icon" class="icon pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="icon640353453" data-review-reference-id="icon640353453">\
            <div class="stencil-wrapper" style="width: 32px; height: 32px">\
               <div xmlns="http://www.w3.org/1999/xhtml" xmlns:pidoco="http://www.pidoco.com/util" style="width:32px;height:32px;" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" width="32" height="32" class="fill-black">\
                     <!--print symbols here-->\
                     <!--load fonticon glyph-e208-->\
                     <use xlink:href="#icon-glyph-e208"></use>\
                  </svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 32px; height: 32px"></div>\
            <div xmlns:json="http://json.org/" xmlns="http://www.w3.org/1999/xhtml"><script type="text/javascript">\
				$(document).ready(function(){\
					rabbit.errorContext(function () {\
						rabbit.interaction.manager.registerInteraction(\'__containerId__-page696252306-layer-icon640353453\', \'interaction712787574\', {"button":"left","id":"action963734329","numberOfFinger":"1","type":"click"},  \
							[\
								{"delay":"0","id":"reaction715411420","type":"hideOverlay"}\
							]\
						);\
					});\
				});\
			</script></div>\
         </div>\
         <div id="__containerId__-page696252306-layer-checkbox573720874" style="position: absolute; left: 165px; top: 378px; width: 91px; height: 22px" data-stencil-type="default.checkbox" data-interactive-element-type="default.checkbox" class="checkbox stencil mobile-interaction-potential-trigger " data-stencil-id="checkbox573720874" data-review-reference-id="checkbox573720874">\
            <div class="stencil-wrapper" style="width: 91px; height: 22px">\
               <div xmlns="http://www.w3.org/1999/xhtml" class="stencil-checkbox-disabled" style="font-size:1em;" xml:space="preserve" title="">\
                  			\
                  <nobr>\
                     				<label>\
                        					<input id="__containerId__-page696252306-layer-checkbox573720874input" xml:space="default" style="padding-right:9px" type="checkbox" disabled="disabled" />\
                        					\
                        						\
                        						Verified User\
                        						\
                        					\
                        				</label>\
                     			\
                  </nobr>\
                  		\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page696252306-layer-iphoneButton785822252" style="position: absolute; left: 185px; top: 420px; width: 50px; height: 30px" data-stencil-type="default.iphoneButton" data-interactive-element-type="default.iphoneButton" class="iphoneButton stencil mobile-interaction-potential-trigger " data-stencil-id="iphoneButton785822252" data-review-reference-id="iphoneButton785822252">\
            <div class="stencil-wrapper" style="width: 50px; height: 30px">\
               <div xmlns="http://www.w3.org/1999/xhtml" xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 30px;width:50px;">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" overflow="hidden" width="50" height="30" viewBox="0 0 50 30"><a>\
                        <path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,29.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-20 0.5,-2 1,-1 1,-1 2,-0.5 40,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,20 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></path>\
                        <text x="25" y="15" dy="0.3em" fill="#FFFFFF" style="font-size:1em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">Verify</text></a></svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page696252306"] .border-wrapper,\
         		body[data-current-page-id="page696252306"] .simulation-container {\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page696252306"] .border-wrapper,\
         		body.has-frame[data-current-page-id="page696252306"] .simulation-container {\
         			height:660px;\
         		}\
         		\
         		body[data-current-page-id="page696252306"] .svg-border-1366-660 {\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page696252306"] .border-wrapper .border-div {\
         			width:1366px;\
         			height:660px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page696252306",\
      			"name": "UserSettingsOverlay",\
      			"layers": {\
      				\
      					"layer483188662":false,\
      					"layer10959018":false,\
      					"layer393887861":false\
      			},\
      			"image":"../resources/icons/no_image.png",\
      			"width":1366,\
      			"height":660,\
      			"parentFolder": "",\
      			"frame": "browser",\
      			"frameOrientation": "landscape",\
      			"renderAboveLayer": ""\
      		}\
      	\
   </div>\
</div>');