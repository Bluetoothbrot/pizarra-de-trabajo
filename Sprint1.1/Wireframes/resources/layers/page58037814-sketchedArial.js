rabbit.data.layerStore.addLayerFromHtml('<div xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" id="__containerId__page58037814-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page58037814" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page58037814-layer-rect123633483" style="position: absolute; left: 110px; top: 20px; width: 1145px; height: 600px" data-stencil-type="static.rect" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect123633483" data-review-reference-id="rect123633483">\
            <div class="stencil-wrapper" style="width: 1145px; height: 600px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" overflow="hidden" style="height: 600px; width:1145px;" width="1145" height="600" viewBox="0 0 1145 600">\
                     <g width="1145" height="600">\
                        <rect x="0" y="0" width="1145" height="600" style="stroke-width:1;stroke:black;fill:white;"></rect>\
                     </g>\
                  </svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-rect399576361" style="position: absolute; left: 110px; top: 20px; width: 1145px; height: 60px" data-stencil-type="static.rect" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect399576361" data-review-reference-id="rect399576361">\
            <div class="stencil-wrapper" style="width: 1145px; height: 60px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" overflow="hidden" style="height: 60px; width:1145px;" width="1145" height="60" viewBox="0 0 1145 60">\
                     <g width="1145" height="60">\
                        <rect x="0" y="0" width="1145" height="60" style="stroke-width:1;stroke:black;fill:white;"></rect>\
                     </g>\
                  </svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-text946864312" style="position: absolute; left: 590px; top: 30px; width: 79px; height: 37px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text946864312" data-review-reference-id="text946864312">\
            <div class="stencil-wrapper" style="width: 79px; height: 37px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:89px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12">\
                        <p><span style="font-size: 32px;">Job 1</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-map224208331" style="position: absolute; left: 140px; top: 190px; width: 400px; height: 400px" data-stencil-type="static.map" data-interactive-element-type="static.map" class="map stencil mobile-interaction-potential-trigger " data-stencil-id="map224208331" data-review-reference-id="map224208331">\
            <div class="stencil-wrapper" style="width: 400px; height: 400px">\
               <div xmlns="http://www.w3.org/1999/xhtml" style="background:white;width:394px; height:394px;border:none" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" overflow="hidden" style="height: 400px;width:400px;" width="400" height="400">\
                     <g id="__containerId__-page58037814-layer-map224208331svg" width="400" height="400">\
                        <path xmlns="" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 1.31, 22.84, 1.05 Q 33.26, 1.40, 43.68, 0.91 Q 54.11, 1.68, 64.53, 2.38 Q 74.95, 1.56, 85.37, 1.64 Q 95.79, 1.15, 106.21, 2.32 Q 116.63, 2.39, 127.05, 2.18 Q 137.47, 1.51, 147.89, 0.90 Q 158.32, 0.94, 168.74, 1.01 Q 179.16, 0.44, 189.58, 0.86 Q 200.00, 1.35, 210.42, 1.91 Q 220.84, 1.97, 231.26, 1.60 Q 241.68, 1.84, 252.11, 1.23 Q 262.53, 0.13, 272.95, 0.64 Q 283.37, -0.03, 293.79, -0.26 Q 304.21, -0.35, 314.63, 0.11 Q 325.05, 0.64, 335.47, 0.81 Q 345.89, 0.74, 356.32, 0.69 Q 366.74, 1.13, 377.16, 1.22 Q 387.58, 1.26, 398.63, 1.37 Q 398.61, 12.22, 398.46, 22.78 Q 397.52, 33.29, 397.90, 43.69 Q 399.00, 54.09, 399.66, 64.51 Q 399.87, 74.94, 399.95, 85.36 Q 399.78, 95.79, 399.15, 106.21 Q 399.63, 116.63, 399.88, 127.05 Q 400.03, 137.47, 398.56, 147.89 Q 399.50, 158.32, 399.63, 168.74 Q 399.69, 179.16, 399.72, 189.58 Q 399.35, 200.00, 399.84, 210.42 Q 399.97, 220.84, 400.14, 231.26 Q 400.30, 241.68, 400.24, 252.11 Q 399.69, 262.53, 399.60, 272.95 Q 399.28, 283.37, 399.14, 293.79 Q 399.28, 304.21, 398.86, 314.63 Q 398.05, 325.05, 397.93, 335.47 Q 398.65, 345.89, 398.24, 356.32 Q 398.02, 366.74, 398.86, 377.16 Q 399.13, 387.58, 398.51, 398.51 Q 387.75, 398.52, 377.28, 398.84 Q 366.82, 399.26, 356.36, 399.49 Q 345.90, 398.53, 335.48, 398.38 Q 325.05, 398.33, 314.63, 398.45 Q 304.21, 399.43, 293.79, 399.82 Q 283.37, 399.65, 272.95, 399.65 Q 262.53, 399.30, 252.11, 399.21 Q 241.68, 398.74, 231.26, 399.26 Q 220.84, 399.65, 210.42, 399.53 Q 200.00, 398.59, 189.58, 398.46 Q 179.16, 398.75, 168.74, 398.54 Q 158.32, 398.78, 147.89, 399.02 Q 137.47, 399.13, 127.05, 399.47 Q 116.63, 399.03, 106.21, 397.55 Q 95.79, 398.06, 85.37, 398.93 Q 74.95, 399.10, 64.53, 398.74 Q 54.11, 399.23, 43.68, 399.21 Q 33.26, 398.25, 22.84, 399.06 Q 12.42, 398.06, 1.66, 398.34 Q 0.95, 387.93, 0.68, 377.35 Q 0.45, 366.84, 0.37, 356.37 Q 1.09, 345.91, 0.73, 335.48 Q 0.43, 325.06, 0.38, 314.63 Q 0.50, 304.21, 0.11, 293.79 Q 0.32, 283.37, 0.82, 272.95 Q 0.84, 262.53, 1.55, 252.11 Q 2.01, 241.68, 1.54, 231.26 Q 1.72, 220.84, 1.51, 210.42 Q 1.16, 200.00, 1.56, 189.58 Q 1.56, 179.16, 1.97, 168.74 Q 1.41, 158.32, 1.77, 147.89 Q 0.76, 137.47, 1.30, 127.05 Q 0.93, 116.63, 0.64, 106.21 Q 0.46, 95.79, 0.82, 85.37 Q 1.47, 74.95, 1.17, 64.53 Q 1.24, 54.11, 0.62, 43.68 Q 1.62, 33.26, 1.18, 22.84 Q 2.00, 12.42, 2.00, 2.00" style=" fill:white;"></path>\
                     </g>\
                  </svg><iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAI1_wY5UgnSSkAM7zVazKOKqpj_GwV6dQ&amp;q=19.0535025,-98.2755407&amp;zoom=15&amp;maptype=roadmap" style="position:absolute;left:4px;top:4px;;width:392px;height:392px;border:none;scrolling:no;"></iframe></div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-text797164243" style="position: absolute; left: 150px; top: 95px; width: 1110px; height: 48px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text797164243" data-review-reference-id="text797164243">\
            <div class="stencil-wrapper" style="width: 1110px; height: 48px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title=""><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textblock2">\
                        <p style="font-size: 14px;">Description - <br />Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna\
                           aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea\
                           takimata sanctus est. \
                        </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-image650657812" style="position: absolute; left: 620px; top: 165px; width: 80px; height: 60px" data-stencil-type="default.image" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="image650657812" data-review-reference-id="image650657812">\
            <div class="stencil-wrapper" style="width: 80px; height: 60px">\
               <div xmlns="http://www.w3.org/1999/xhtml" xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg class="image-cropper" style="height: 60px;width:80px;" width="80" height="60" viewBox="0 0 80 60">\
                     <g width="80" height="60">\
                        <rect x="0" y="0" width="80" height="60" style="stroke:black; stroke-width:1;fill:white;"></rect>\
                        <line x1="0" y1="0" x2="80" y2="60" style="stroke:black; stroke-width:0.5;"></line>\
                        <line x1="0" y1="60" x2="80" y2="0" style="stroke:black; stroke-width:0.5;"></line>\
                     </g>\
                  </svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-text796787769" style="position: absolute; left: 740px; top: 185px; width: 116px; height: 17px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text796787769" data-review-reference-id="text796787769">\
            <div class="stencil-wrapper" style="width: 116px; height: 17px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title=""><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.text2">\
                        <p style="font-size: 14px;">Name of Employer</p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-rating179882676" style="position: absolute; left: 905px; top: 180px; width: 150px; height: 30px" data-stencil-type="default.rating" data-interactive-element-type="default.rating" class="rating stencil mobile-interaction-potential-trigger " data-stencil-id="rating179882676" data-review-reference-id="rating179882676">\
            <div class="stencil-wrapper" style="width: 150px; height: 30px">\
               <div xmlns="http://www.w3.org/1999/xhtml" style="width:150px; height:30px" onmouseout="if(rabbit.stencils.rating.checkMouseOutDiv(\'__containerId__-page58037814-layer-rating179882676\', event)) rabbit.facade.raiseEvent(rabbit.events.ratingMouseOut, \'__containerId__-page58037814-layer-rating179882676\');" title=""><img id="__containerId__-page58037814-layer-rating179882676-1" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 0px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page58037814-layer-rating179882676\', \'1\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page58037814-layer-rating179882676\', \'1\');" /><img id="__containerId__-page58037814-layer-rating179882676-2" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 30px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page58037814-layer-rating179882676\', \'2\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page58037814-layer-rating179882676\', \'2\');" /><img id="__containerId__-page58037814-layer-rating179882676-3" src="../resources/icons/rating_black.png" width="30" height="30" style="position: absolute; left: 60px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page58037814-layer-rating179882676\', \'3\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page58037814-layer-rating179882676\', \'3\');" /><img id="__containerId__-page58037814-layer-rating179882676-4" src="../resources/icons/rating_white.png" width="30" height="30" style="position: absolute; left: 90px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page58037814-layer-rating179882676\', \'4\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page58037814-layer-rating179882676\', \'4\');" /><img id="__containerId__-page58037814-layer-rating179882676-5" src="../resources/icons/rating_white.png" width="30" height="30" style="position: absolute; left: 120px; cursor: pointer;" onmouseover="rabbit.facade.raiseEvent(rabbit.events.ratingMouseOver, \'__containerId__-page58037814-layer-rating179882676\', \'5\');" onclick="rabbit.facade.raiseEvent(rabbit.events.ratingResultChangedEvent, \'__containerId__-page58037814-layer-rating179882676\', \'5\');" /></div><script xmlns="http://www.w3.org/1999/xhtml" type="text/javascript">rabbit.errorContext(function () {\
			rabbit.stencils.rating.onLoad("__containerId__-page58037814-layer-rating179882676", "3");\
		});</script></div>\
         </div>\
         <div id="__containerId__-page58037814-layer-text153802247" style="position: absolute; left: 620px; top: 255px; width: 299px; height: 30px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text153802247" data-review-reference-id="text153802247">\
            <div class="stencil-wrapper" style="width: 299px; height: 30px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:309px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textblock2">\
                        <p style="font-size: 14px;"><span style="font-size: 26px;">Date - Duration - Location</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-icon313065689" style="position: absolute; left: 1000px; top: 35px; width: 32px; height: 32px" data-stencil-type="fonticon.icon" data-interactive-element-type="fonticon.icon" class="icon stencil mobile-interaction-potential-trigger " data-stencil-id="icon313065689" data-review-reference-id="icon313065689">\
            <div class="stencil-wrapper" style="width: 32px; height: 32px">\
               <div xmlns="http://www.w3.org/1999/xhtml" xmlns:pidoco="http://www.pidoco.com/util" style="width:32px;height:32px;" title="">\
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" width="32" height="32" class="fill-black">\
                     <svg style="position: absolute; width: 0; height: 0;" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e228" preserveAspectRatio="xMidYMid meet">\
\
<path d="M249.241 613.549h142.262q0 90.315 87.375 116.13v-197.863q-2.621-1.033-8.103-2.383t-8.103-2.304q-24.148-6.911-42.495-13.186t-40.191-16.283-37.73-20.813-31.612-26.689-25.498-33.919-15.886-42.495-6.117-52.821q0-39 11.915-72.758t32.25-58.304 48.134-42.813 58.78-28.992 64.658-14.218v-52.982h66.246v53.616q40.986 4.291 74.586 14.932t62.274 29.786 48.293 46.15 30.582 65.849 10.961 87.534h-142.658q-1.351-26.769-5.322-44.799t-13.027-33.282-25.181-24.464-40.51-13.583v172.763q14.853 1.668 22.639 3.496t13.9 4.448 11.121 3.972q47.341 12.869 69.502 22.161 133.048 55.284 133.048 189.286 0 46.705-16.045 86.581t-46.864 70.455-78.637 49.882-108.662 23.83v51.948h-66.246v-53.616q-47.659-5.959-89.837-26.372t-73.316-51.789-48.93-74.428-17.554-91.665zM405.402 307.737q0 28.119 14.932 42.972t58.543 29.786v-152.191q-32.091 5.958-52.744 25.656t-20.731 53.775zM545.124 735q51.63-4.607 75.62-24.307t23.988-63.705q0-36.777-23.83-57.429t-75.779-34.951v180.39z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-270f" preserveAspectRatio="xMidYMid meet">\
\
<path d="M115.477 868.048l78.161-203.584 135.035 135.034-203.584 78.161q-6.275 2.621-9.214-0.16t-0.397-9.453zM234.624 623.479l424.959-424.641 135.035 134.715-424.959 425.277zM700.333 157.771l67.516-67.516q6.911-6.911 16.84-6.911t16.918 6.911l101.277 101.277q6.911 6.99 6.911 16.918t-6.911 16.84l-67.516 67.516-16.918-16.839-101.277-101.276z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e208" preserveAspectRatio="xMidYMid meet">\
\
<path d="M214.133 681.384q0-7.625 5.639-13.264l186.984-186.984-186.984-186.984q-5.639-5.639-5.639-13.583t5.639-13.264l78.797-78.717q5.561-5.322 13.186-5.322t13.264 5.322l187.299 186.984 186.984-186.984q5.322-5.322 13.264-5.322t13.264 5.322l78.717 78.717q5.322 5.322 5.322 13.264t-5.322 13.583l-186.665 186.984 186.665 186.665q5.322 5.639 5.322 13.424t-5.322 13.424l-78.717 78.717q-5.322 5.639-13.264 5.639t-13.583-5.639l-186.665-186.665-186.984 186.984q-5.322 5.322-13.264 5.322t-13.583-5.322l-78.717-79.114q-5.639-5.561-5.639-13.185z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e004" preserveAspectRatio="xMidYMid meet">\
\
<path d="M131.364 828.65v-77.764q0-26.451 6.832-39.875t26.292-24.307q159.181-92.061 231.704-123.515v-124.388q-33.124-24.544-33.124-74.191v-98.575q0-41.702 16.045-74.824t50.519-53.617 82.371-20.493 82.45 20.493 50.439 53.617 16.045 74.824v98.575q0 49.327-33.124 73.793v124.788q61.559 25.181 231.704 123.515 19.539 10.883 26.292 24.307t6.832 39.875v77.764q0 6.99-4.846 11.756t-11.756 4.765h-728.070q-6.674 0-11.597-4.765t-5.004-11.756z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e345" preserveAspectRatio="xMidYMid meet">\
\
<path d="M114.842 613.549v-430.281h132.414q13.583 0 23.273 9.77t9.77 23.353v364.035q0 13.583-9.77 23.353t-23.273 9.77h-132.414zM313.421 514.259v-264.826q0-27.086 19.539-46.626t46.626-19.539h50.677l52.266-26.134q13.264-6.99 29.469-6.99h165.457q16.601 0 31.139 7.784t23.83 21.685l129.076 193.336 28.119 28.437q19.539 18.825 19.539 46.626v66.246q0 27.482-19.382 46.785t-46.785 19.382h-217.167l17.237 86.421q1.351 5.561 1.351 12.869v99.289q0 27.482-19.382 46.864t-46.864 19.301h-33.044q-18.904 0-34.792-9.929t-24.464-26.769l-63.546-126.773-95.636-127.726q-13.264-17.872-13.264-39.716zM379.586 514.259l99.289 132.332 66.246 132.414h33.044v-99.289l-33.044-165.457h297.867v-66.246l-33.123-33.044-132.414-198.579h-165.457l-66.167 33.044h-66.246v264.826z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e028" preserveAspectRatio="xMidYMid meet">\
\
<path d="M123.103 406.312q0-63.863 25.021-121.928t67.041-100.163 100.242-66.96 122.165-25.021 121.927 25.021 100.083 66.959 67.041 100.163 25.022 121.927q0 92.061-50.36 170.143l194.607 194.607q5.004 4.607 5.004 11.597t-5.004 11.915l-70.455 70.455q-4.607 4.687-11.756 4.687t-11.756-4.687l-194.607-194.607q-77.445 50.36-169.745 50.36-63.942 0-122.165-25.022t-100.242-67.041-67.041-100.242-25.021-122.165zM223.106 406.312q0 43.687 16.999 83.404t45.833 68.39 68.39 45.673 83.244 16.999 83.165-16.999 68.39-45.673 45.833-68.39 17.077-83.404q0-43.29-17.077-83.007t-45.833-68.39-68.39-45.672-83.165-16.999-83.244 16.999-68.39 45.672-45.833 68.39-17.077 83.007z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e344" preserveAspectRatio="xMidYMid meet">\
\
<path d="M114.842 779.006v-430.281h132.414q13.583 0 23.273 9.77t9.77 23.353v364.035q0 13.583-9.77 23.353t-23.273 9.77h-132.414zM313.421 679.716v-264.745q0-21.844 13.264-39.716l95.636-127.805 63.545-126.773q8.578-16.84 24.464-26.769t34.791-9.929h33.044q27.482 0 46.864 19.382t19.382 46.785v99.289l-18.587 99.289h217.167q27.482 0 46.785 19.382t19.382 46.864v66.167q0 27.801-19.539 46.705l-28.436 28.755-128.759 192.938q-9.295 13.9-23.83 21.685t-31.139 7.784h-165.456q-15.886 0-29.469-6.911l-52.266-26.212h-50.677q-27.403 0-46.785-19.301t-19.382-46.864zM379.586 679.716h66.246l66.167 33.124h165.457l132.414-198.579 33.124-33.123v-66.167h-297.867l33.044-165.536v-99.289h-33.044l-66.246 132.414-99.289 132.414v264.745z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e195" preserveAspectRatio="xMidYMid meet">\
\
<path d="M106.58 481.138q0-55.284 14.535-107.709t40.748-96.986 63.545-81.894 81.894-63.545 97.144-40.748 107.552-14.535q82.45 0 157.513 32.091t129.474 86.343 86.343 129.474 32.091 157.513-32.091 157.513-86.343 129.474-129.474 86.343-157.513 32.091q-54.969 0-107.552-14.535t-97.144-40.748-81.894-63.545-63.545-81.894-40.748-96.985-14.535-107.709zM352.819 389.79q0 3.652 2.304 5.959 1.987 2.621 5.958 2.621h89.043q7.625 0 8.262-7.548 1.668-24.544 15.569-37.095t39.081-12.629q17.872 0 29.071 10.645t11.28 27.166q0 9.214-4.291 18.031t-11.28 15.33-10.247 9.294-6.592 5.004q-58.62 42.656-58.62 95.318v33.759q0 1.589 3.337 4.926t4.926 3.337h84.755q3.337 0 5.799-2.304t2.463-5.639q0.635-31.139 9.612-47.977t33.124-32.487q16.203-10.247 24.464-16.204t16.521-14.218 11.597-17.237 5.322-21.368 1.986-30.264q0-31.773-13.424-56.872t-35.745-40.43-49.009-22.955-55.046-7.784q-72.839 0-114.699 37.015t-45.514 104.611zM445.833 729.359q0 6.99 4.765 11.756t11.756 4.765h100.957q6.592 0 11.597-4.765t4.926-11.756v-99.289q0-6.911-4.926-11.756t-11.597-4.765h-100.957q-6.911 0-11.756 4.765t-4.765 11.756v99.289z"/>\
</symbol><symbol xmlns:a0="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" id="icon-glyph-e340" preserveAspectRatio="xMidYMid meet">\
\
<path d="M82.434 442.452q-1.351-19.223 9.93-37.73t32.091-22.875q32.726-6.911 62.037-1.668t40.908 21.208q36.38-26.531 81.894-35.269t91.028-0.953 91.187 29.23q61.242 29.152 87.057 37.413 27.404 8.659 42.336 5.958 10.564-1.987 18.19-6.275t12.075-10.723 7.148-12.075 5.799-13.9 5.4-12.948q-30.739 1.351-64.658-3.652t-60.766-13.027-49.168-16.283-34.551-14.059l-11.915-5.639q-3.972-1.589-9.929-4.765t-16.362-11.915-11.915-17.237 13.583-18.984 50.121-18.904q19.857-4.607 42.020-5.242t43.529 2.621 42.020 8.42 40.51 12.629 36.3 14.376 31.612 14.853 23.988 12.948 16.045 9.612l1.27-0.715q-10.883-10.883-25.099-23.512t-43.051-32.408-57.747-33.919-67.516-22.32-74.268-3.337q-4.687-4.291-8.817-9.453t-7.229-11.201-3.972-11.915 2.145-11.121 9.929-8.897 20.652-5.481 33.282-0.477q26.134 1.589 54.094 11.201t51.472 23.669 45.991 30.66 39.716 32.886 30.264 29.787 20.017 21.685l6.99 8.261q3.652 2.383 20.017 8.5t39.397 18.666 49.486 33.124q23.114 18.19 34.235 28.437t16.918 21.526 4.131 21.368-8.975 26.292q-4.291 10.247-8.738 16.84t-7.625 8.975-6.275 2.778-4.527-0.16-2.621-1.668q14.218 15.886 1.351 26.212-11.28 8.897-28.993 12.233t-35.904 2.939-36.062-0.635-33.282 3.099-23.669 12.788q-15.886 17.237-32.091 44.482t-28.277 52.346-30.66 55.761-38.047 53.617q5.004 1.987 13.9 4.291t16.443 4.448 14.535 4.846 10.883 6.275 3.178 7.944-2.778 7.944-5.959 5.799-7.625 3.812-9.77 2.145-10.247 0.953-11.597 0.16-11.121-0.635-11.041-0.874-9.612-0.795q-67.516 53.299-96.35 54.969-12.55 0.953-11.915-8.975 1.033-14.853 25.814-36.38 5.322-5.004 11.597-9.612-16.204-1.668-56.078-1.351t-71.489-2.463-49.804-12.39q-15.886-8.578-30.264-25.181t-23.353-32.091-21.208-31.773-24.148-23.83q-6.592-4.291-19.064-9.77t-22.161-10.405-20.493-12.55-18.507-19.699-11.756-28.357q-10.961-43.687-11.439-57.589t8.42-61.56q-1.987-6.911-9.294-7.944t-15.886 1.509-18.19 3.018-16.521-3.496q-11.28-7.309-12.55-26.451zM370.691 598.616q2.939 1.033 4.607 1.668t8.659 4.765 11.915 9.135 12.075 14.535 11.915 21.208 8.578 29.152 4.448 38.048q12.947 3.257 54.969 12.233t72.122 17.237q8.262-15.251 12.39-33.124t4.21-32.567-1.192-27.325-3.177-19.539l-1.668-7.309q-2.939 1.351-8.261 3.496t-21.286 5.959-31.455 4.607-35.745-3.972-37.254-16.362q-35.109-24.148-60.925-23.195-4.926 0.397-4.926 1.351zM801.926 389.79q-3.337 8.578 2.145 17.554t16.759 13.264q10.883 3.972 20.813 0.795t13.264-11.756-2.304-17.554-16.601-12.868q-10.883-4.291-20.971-1.192t-13.107 11.756z"/>\
</symbol></defs></svg>\
                     <!--load fonticon glyph-e228-->\
                     <use xlink:href="#icon-glyph-e228"></use>\
                  </svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-text989091222" style="position: absolute; left: 1050px; top: 45px; width: 25px; height: 17px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text989091222" data-review-reference-id="text989091222">\
            <div class="stencil-wrapper" style="width: 25px; height: 17px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:35px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.text2">\
                        <p style="font-size: 14px;">Pay</p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-text108109916" style="position: absolute; left: 615px; top: 315px; width: 146px; height: 17px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text108109916" data-review-reference-id="text108109916">\
            <div class="stencil-wrapper" style="width: 146px; height: 17px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:156px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.text2">\
                        <p style="font-size: 14px;">Skills / Profile Required</p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page58037814-layer-text578520828" style="position: absolute; left: 625px; top: 345px; width: 47px; height: 48px" data-stencil-type="default.text2" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text578520828" data-review-reference-id="text578520828">\
            <div class="stencil-wrapper" style="width: 47px; height: 48px">\
               <div xmlns="http://www.w3.org/1999/xhtml" title="" style="width:57px;width:-webkit-max-content;width:-moz-max-content;width:max-content;"><span class="default-text2-container-wrapper default-text2-version2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textList2">\
                        <ul>\
                           \
                           <li>Skill</li>\
                           \
                           <li>Skill</li>\
                           \
                           <li>Skill</li>\
                           \
                        </ul></span></span></div>\
            </div>\
         </div>\
         <div xmlns:json="http://json.org/" xmlns="http://www.w3.org/1999/xhtml"><script type="text/javascript">\
				$(document).ready(function(){\
					rabbit.errorContext(function () {\
						rabbit.interaction.manager.registerInteraction(\'__containerId__page58037814-layer\', \'interaction19526136\', {"button":"left","id":"action174006299","numberOfFinger":"1","type":"click"},  \
							[\
								{"delay":"0","id":"reaction647844499","type":"hideOverlay"}\
							]\
						);\
					});\
				});\
			</script></div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page58037814"] .border-wrapper,\
         		body[data-current-page-id="page58037814"] .simulation-container {\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page58037814"] .border-wrapper,\
         		body.has-frame[data-current-page-id="page58037814"] .simulation-container {\
         			height:660px;\
         		}\
         		\
         		body[data-current-page-id="page58037814"] .svg-border-1366-660 {\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page58037814"] .border-wrapper .border-div {\
         			width:1366px;\
         			height:660px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page58037814",\
      			"name": "jobDetailOverlay",\
      			"layers": {\
      				\
      					"layer483188662":false,\
      					"layer10959018":false,\
      					"layer393887861":false\
      			},\
      			"image":"../resources/icons/no_image.png",\
      			"width":1366,\
      			"height":660,\
      			"parentFolder": "",\
      			"frame": "browser",\
      			"frameOrientation": "landscape",\
      			"renderAboveLayer": "layer393887861"\
      		}\
      	\
   </div>\
</div>');