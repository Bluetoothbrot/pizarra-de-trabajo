<head><title>Sprint 1</title></head>
<h1 id="cover-page" align="center"> Software Engineering Fall 2019 </h1>
<h2 align="center"> Dra. Ofelia Cervantes </h2>


# Sprint 1

Members |    |  >
-- | -- | --
154816 Jose Angel Hernandez Morales | 157130 Mariella Sanchez Alvarez | 54409 Antonio Rafael Bravo Pacheco
155150 Jose Carlos Archundia Adriano | 161035 Hector Eduardo Cardona Espinoza | 403028 Oliver Maicher 

<br />
<br />

<!-- pagebreak --><P style="page-break-before: always">

# Table of Contents
- [Sprint 1](#sprint-1)
- [Table of Contents](#table-of-contents)
- [Company Description](#company-description)
  - [Presentation](#presentation)
  - [Capacities and Roles](#capacities-and-roles)
  - [Version Management Tools](#version-management-tools)
  - [Project Management Tools](#project-management-tools)
  - [Hardware and Software Selected for Implementing the Project](#hardware-and-software-selected-for-implementing-the-project)
- [Artifacts - Kickoff](#artifacts---kickoff)
  - [Context](#context)
  - [Proposal](#proposal)
  - [Executive Summary](#executive-summary)
  - [Proto-Personas](#proto-personas)
    - [Employer](#employer)
    - [Employee](#employee)
  - [Story Boards](#story-boards)
    - [Employer:](#employer)
    - [Employee:](#employee)
- [Product Backlog](#product-backlog)
- [Artifacts: Sprint 1](#artifacts-sprint-1)
  - [Use Case Diagram](#use-case-diagram)
  - [UML Diagrams](#uml-diagrams)
    - [Sequence](#sequence)
    - [Activities](#activities)
    - [State](#state)
    - [Class](#class)
  - [Low Fidelity Prototype](#low-fidelity-prototype)
  - [Information Flow Diagram](#information-flow-diagram)
  - [General System Architecture](#general-system-architecture)
  - [Technical Documentation](#technical-documentation)
  - [Demo Videos](#demo-videos)
- [Retrospective](#retrospective)
  - [Kanban Board](#kanban-board)
  - [Member retrospectives](#member-retrospectives)
    - [157130 Mariella Sánchez Álvarez](#157130-mariella-s%c3%a1nchez-%c3%81lvarez)
    - [155150 Jose Carlos Archundia Adriano](#155150-jose-carlos-archundia-adriano)
    - [154409 Antonio Rafael Bravo Pacheco](#154409-antonio-rafael-bravo-pacheco)
    - [154816 Jose Angel Hernández Morales](#154816-jose-angel-hern%c3%a1ndez-morales)
    - [161035 Héctor Eduardo Cardona Espinoza](#161035-h%c3%a9ctor-eduardo-cardona-espinoza)
    - [403028 Oliver Maicher](#403028-oliver-maicher)
  - [Sprint 2 Backlog](#sprint-2-backlog)
- [Appendix](#appendix)

<!-- pagebreak<P style="page-break-before: always"> -->

# Company Description

## Presentation
_At Aguacate, we aim to provide software that caters to our clients needs. We believe in fast, efficient software that not only meets our clients needs, but also enables them to do their best work. Our team of experienced developers is able to pinpoint exactly what our client needs, and create an adequate solution for any problem._

## Capacities and Roles
* Scrum Master  -  Jose Carlos
* Product Owner -  Mariella

## Version Management Tools
[GitLab](https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo) - For collaboration and management  
![GitLab Members](../Artifacts/img/gitlabMembers.png)

## Project Management Tools
[GitLab](https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo) - For task assignment and organization using Kanban  
[Asana](https://app.asana.com/0/1141471826392262/board) - Complement to GitLab Kanban  
![Asana Members](../Artifacts/img/AsanaMembers.png)

## Hardware and Software Selected for Implementing the Project
Atom - Used as a lightweight code editor  
VSCode - Used as a fully-featured code editor and IDE  
Team computers and cell phones - Used by the team to develop and collaborate on the software  
Pidoco - Used for creating wireframes  
Draw.io - Used for creating diagrams

<br />


# Artifacts - Kickoff

## Context
Carlos is a student that wants some extra money, but does not want to commit to a full-time job.  
Daniel is a restaurant owner who needs an extra worker for an event he is hosting, but only wants to hire for a day or two.

## Proposal
We propose a web-based platform where we can connect two types of users: Employers and Employees. Our platform provides a bridge between these two users that allows them to coordinate short time job listings, similar to the way a Temp Agency functions.

## Executive Summary
The objective of our software product is to create a platform where users can post or apply to work proposals. Users called Employers, users who post work, can choose between the different users called Employees, users who apply to work, who are interested in doing the work. Our goal is to guarantee the Employers a suitable Employee who will complete the work given to them, and guarantee the Employee his payment. To do this,we will manage payments on our platform, asking Employers to make a deposit beforehand. This deposit will be help until the verification of completed work. If the task assigned to the Employee is completed successfully, the money deposited by the Employer will be transferred to the Employee, otherwise, either the Employee or the Employer can open up a claim to dispute the payment.  Our platform will allow Employers to filter through Employee candidates with suitable skills to the job the Employer has posted. We aim to provide a secure platform that enables a Employer to connect with an Employee to realize a task suitable to the Employee’s skill set.

<!-- pagebreak<P style="page-break-before: always"> -->

## Proto-Personas

### Employer
* Daniel
* 30
* Male
* Restaurant Manager
* Good with technology
* Has enough money to hire others to work for him
* Organizing Octoberfest in Mexico
* Needs reliable workers for the evening



### Employee
* Carlos
* 23
* Male
* In need of money for vacation
* Looking for a weekend job
* Tech savy
* Physically able
* Not picky with work
* Has transport

<!-- pagebreak<P style="page-break-before: always"> -->

## Story Boards
### Employer:  
![Employer Story board](../Artifacts/Storyboards/storyboardEmployer.jpeg)  
In this story board we illustrate the process of a Employer posting a job listing on our platform. The Employer is in need of an employee and posts a job offer to our platform, after which he receives an Employee which fulfills his requirements.  

### Employee:  
![Employee Story board](../Artifacts/Storyboards/storyboardEmployee.png)  
In this story board we illustrate the process of an Employee looking for suitable work on our platform. The Employee is in need of money and consults our platform for jobs that are suitable for him, after which he applies and is accepted to a job where he will be paid.

<!-- pagebreak<P style="page-break-before: always"> -->

# [Product Backlog](../Artifacts/ProductBacklog/PRODUCTBACKLOG.md)
[{{../Artifacts/ProductBacklog/PRODUCTBACKLOG.md}}](../Artifacts/ProductBacklog/PRODUCTBACKLOG.md)

<object type="text/html"
    data="../Artifacts/ProductBacklog/PRODUCTBACKLOG.md"
    width="800"
    height="400">
</object>



<!-- pagebreak<P style="page-break-before: always"> -->


# Artifacts: Sprint 1

## Use Case Diagram
![Use Case Diagram](UseCaseDiagram/useCaseDiagram.png)


## UML Diagrams

### Sequence
![Sequence Diagram](UML/sequenceDiagram/sequenceDiagram.png)

### Activities
![Activities Diagram](UML/ActivityDiagram/activityDiagram.png)

### State
![State Diagram](UML/stateDiagram/stateDiagram.png)

### Class
![Class Diagram](UML/classDiagram.png)


<!-- pagebreak<P style="page-break-before: always"> -->

## Low Fidelity Prototype
[Pidoco Prototype](https://pidoco.com/rabbit/invitation/SwTsIUPbrYcxQGC9hC6y0I7hW2SN5g0zGXfps02r)

## Information Flow Diagram
Employer:  
![Employer Flow Diagram](flowDiagram/flowDiagramEmployer.jpeg)  
Employee:  
![Employee Flow Diagram](flowDiagram/flowDiagramEmployee.jpeg)  

## General System Architecture
![General System Architecture](UML/GeneralSystemArchitecture.png)

## Technical Documentation
[ Technical Documentation ](Technical_documentation.pdf)   
<object type="application/pdf"
    data="Technical_documentation.pdf"
    width="800"
    height="400">
</object>

## Demo Videos
[Video 1](https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo/raw/master/Sprint1.1/DemoVideo/Video1.webm)  
<embed type="video/webm"
       src="https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo/raw/master/Sprint1.1/DemoVideo/Video1.webm"
       width="250"
       height="200">
<!-- <iframe
    title="Video 1"
    width="300"
    height="200"
    src="https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo/raw/master/Sprint1.1/DemoVideo/Video1.webm">
</iframe> -->

[Video 2](https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo/raw/master/Sprint1.1/DemoVideo/Video2.webm)  
<embed type="video/webm"
       src="https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo/raw/master/Sprint1.1/DemoVideo/Video2.webm"
       width="250"
       height="200">

[Video 3]([DemoVideo/Video3.webm](https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo/raw/master/Sprint1.1/DemoVideo/Video3.webm))  
<embed type="video/webm"
       src="https://gitlab.com/Bluetoothbrot/pizarra-de-trabajo/raw/master/Sprint1.1/DemoVideo/Video3.webm"
       width="250"
       height="200">

<!-- pagebreak<P style="page-break-before: always"> -->

# Retrospective

## Kanban Board
Post-Kickoff:  
![Kickoff Board](../Artifacts/img/kanbanBoardKickoff.png)  
Sprint 1:  
![Sprint1 Board](../Artifacts/img/kanbanBoardSprint1.png)  

## Member retrospectives

### 157130 Mariella Sánchez Álvarez
“During the process of this part of the project I helped make a part of the use case diagram, also I was tasked to make the state diagram which was important to help us visualize where we wanted to be at with the project. In order to make the process clearer and better, together with my team, we used the asana application to communicate and inform us what task each one had to do and that made things go the way we wanted to.”

### 155150 Jose Carlos Archundia Adriano
“This time I was assigned the task of managing the team, diving up the work and making sure that all tasks would be fulfilled till the date of submission. Besides handling the team dynamics, I was making with the technical documentation, elaborating the sequence diagram and programming of the technical prototype as well. The original plan was to work with react.js but as the parallel development of the identical project in ruby made way faster progress we decided on dropping that idea.”

### 154409 Antonio Rafael Bravo Pacheco
“I was working on the functional prototype which I realized using ruby. There’s some websites you can navigate and jump between, forms to add new job listings, all based on a powerful framework called ruby on rails. It even allows you do automatically generate a database schema which quite took the choice from us.”

### 154816 Jose Angel Hernández Morales
“On this occasion my contribution was to make the UML diagrams, class, activity diagram and contributions in the report in general. In this sprint, I learned that prior planning and design are very useful during implementation, since they help us define a path with which we can achieve our objectives in our software product. I also learned that the choice of tools to implement our product are important, in our case it was decided to use ruby, to make the functional prototype and at least in my case I do not have much experience in ruby, so it is important to make a very evaluation detailed of what we really need for the development of our software product.”

### 161035 Héctor Eduardo Cardona Espinoza
“In this sprint I was tasked to help make some diagrams like the flow diagram where it was important to differentiate between the different users that include the employer and the employee. Also I helped in making the activity diagram that I noticed had some similarities between some other diagrams but that is bound to happen with these many types of diagrams. Lastly I also contributed to making some parts of the report and I compiled the information and characteristics necessary to put together the technical documentation and this included the files we were sharing alongside the development of the project.”

### 403028 Oliver Maicher
“In this sprint I was taking care of the product backlog. So basically the job was the check in all the changes that appeared in the last time. We worked with git and found some interesting errors but luckily we were able to resolve them. We decided on using ruby on rails for the prototype instead of react.js and also sql_lite as a database so far. There may be slight changes in the process of development regarding the database as it was planned to work with mongoDB but that is not a big of a problem. Additionally the use of Asana as a project management tool is quite helpful and makes it a lot easier.”


## Sprint 2 Backlog
ID | Statement Item | State | Priority
---- | -------------- | :---: | :------:
23 | As an employer I need to be able to register a job posting  | TBD | 0
24 | As an employer it must be possible to manage the list of applicants for a job  | TBD | 1
25 | As an employer I need the funcionality of accepting applications  | TBD | 1
26 | As an employer I need to be able to edit my job postings | TBD | 2
27 | As an employee I need to be able to apply for jobs | TBD | 0

<!-- pagebreak --><P style="page-break-before: always">

# Appendix
[ {{../Artifacts/CVs/Curriculúm1.pdf}} ](../Artifacts/CVs/Curriculúm1.pdf)  
<object type="application/pdf"
    data="../Artifacts/CVs/Curriculúm1.pdf"
    width="800"
    height="400">
</object>

[ {{../Artifacts/CVs/CV_154816.pdf}} ](../Artifacts/CVs/CV_154816.pdf)  
<object type="application/pdf"
    data="../Artifacts/CVs/CV_154816.pdf"
    width="800"
    height="400">
</object>

[ {{../Artifacts/CVs/CV_Oliver_Manchier_WS18_19.pdf}} ](../Artifacts/CVs/CV_Oliver_Manchier_WS18_19.pdf)  
<object type="application/pdf"
    data="../Artifacts/CVs/CV_Oliver_Maicher_WS18_19.pdf"
    width="800"
    height="400">
</object>


[ {{../Artifacts/CVs/JoseCarlos.pdf}} ](../Artifacts/CVs/JoseCarlos.pdf)  
<object type="application/pdf"
    data="../Artifacts/CVs/JoseCarlos.pdf"
    width="800"
    height="400">
</object>

[ {{../Artifacts/CVs/Rafael.pdf}} ](../Artifacts/CVs/Rafael.pdf)  
<object type="application/pdf"
    data="../Artifacts/CVs/Rafael.pdf"
    width="800"
    height="400">
</object>




