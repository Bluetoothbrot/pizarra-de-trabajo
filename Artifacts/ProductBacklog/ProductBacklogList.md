# Product backlog
## Employee
As an employee, I am searching for work that I am able to do, that fits in my schedule, that is accessible to me. The work must not be a long time commitment and payment method must also be quick to cash out. I must also be guaranteed my payment.

## Contractor
As a contractor, I am looking for an employee that will realize a specific task. They must be qualified, meeting my needs in experience and skills. The employee is hired with no commitment, and must be able to work according to my schedule. The employee must also be trustworthy, and I need some guarantee that the job assigned to them will be completed.
