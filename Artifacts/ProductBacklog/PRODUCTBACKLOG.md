ID | Statement Item | State | Priority
---- | -------------- | :---: | :------:
1  | As an employer, I need information about the payment methods available | TBD | 4
2  | As an employer, I need to know my employees are trustworthy | TBD | 3
3  | As an employer, I want to know personal information about my employees | TBD | 0
4  | As an employer, I would like to have different payment options available to me | TBD | 4
5  | As an employer , I need to have a guarantee that my job will be completed adequately | TBD | 0
6  | As an employer, I need my employees to have adequate work experience for the job assigned to them | TBD | 0
7  | As an employer, I need employees that are able to work at the time I specify | TBD | 0
8  | As an employer, I want to be able to cancel a job even after the deposit is completed, but before the work is done, with a full reimbursement | TBD | 4
9  | As an employee, I want to know what I will be paid for each job so I can adequately choose one | TBD | 2
10 | As an employee, I want to know the time each job needs to be realized in so I can adequately choose one | TBD | 2
11 | As an employee, I want to know where the job will take place, so I can adequately choose one | TBD | 3
12 | As an employee, I want to be guaranteed payment for my job | TBD | 0
13 | As an employee and employer, I want security in my job to avoid scams and/or theft | TBD | 0
14 | I as a service administrator want to see the most listed job types | TBD | 3
15 | I as a service administrator want to see the best/worst employeers rating | TBD | 3
16 | I as a service administrator want to see best/worst employer rating | TBD | 4
17 | I as a service administrator want to see search results jobs/ requests | TBD | 4
18 | I as a service administrator want to see data investigation | TBD | 3
19 | I as a service administrator want to see general page maintenance | TBD | 0
20 | I as a service administrator want to see monthly statistics | TBD | 5
21 | I as a service administrator want to see using metrics | TBD | 4
22 | I as a service administrator need to be able to see and attend user claims | TBD | 2
23 | As an employer I need to be able to register a job posting  | TBD | 0
24 | As an employer it must be possible to manage the list of applicants for a job  | TBD | 1
25 | As an employer I need the funcionality of accepting applications  | TBD | 1
26 | As an employer I need to be able to edit my job postings | TBD | 2
27 | As an employee I need to be able to apply for jobs | TBD | 0
28 | As an employee I need to be able to view all appropriate job listings | TBD | 0
29 | As an employee I need to be able to verify the acceptance of a job | TBD | 1
30 | As an employee I need to be able to see all job listings  | TBD | 4
31 | I as a service administrator want to see the all listed jobs | TBD | 3
32 | I as a service administrator I need to handle payments| TBD | 0
33 | I as a service administrator need to be able to visualize specific job postings| TBD | 3
34 | I as a service administrator need to provide assistence in general | TBD | 1
35 | I as a service administrator need to be able to adminstrate the whole system | TBD | 1
36 | I as a service administrator need to be able to edit job postings | TBD | 2
