# Problem Context
We have a person who is in need of worker(s). They have a specific job that
they need to get done. It is a spontaneous job that needs to be done only once.
They don't want to have to commit to hiring a person full or part time for this task.
They are in need of a trustworthy worker, who may need to have certain skills to get the job done.

On the other side of the problem, we have a person who is looking for a quick job. They need money,
and are willing to do some extra work to get it, however, they don't want to commit to a full or part time job.
They are not picky with work, and want something that they are able to reasonably do.

* Person who needs a job done
 * spontaneous work
 * no commitment
 * trustworthy workers
 * looking for specific qualities in workers
* Person who wants a short job
 * no long term responsibilities
 * quick pay


# Product backlog
As an ____ I want ____ because ____
## Employee
As an Employee, I am searching for work that I am able to do, that fits in my schedule, and that is accessible to me. The work must not be a long time commitment and payment method must also be quick to cash out. I must also be guaranteed my payment.

## Employer
As a Employer, I am looking for an employee that will realize a specific task. They must be qualified, meeting my needs in experience and skills. The employee is hired with no commitment, and must be able to work according to my schedule. The employee must also be trustworthy, and I need some guarantee that the job assigned to them will be completed.
